package es.adoalonso.gwt.sandbox.client;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Composite;

public class MyTableDnd extends Composite {

	public MyTableDnd() {
		AbsolutePanel tableExamplePanel = new AbsolutePanel();
    tableExamplePanel.setPixelSize(450, 300);
    initWidget(tableExamplePanel);
    
    // instantiate our drag controller
    FlexTableRowDragController tableRowDragController = new FlexTableRowDragController(
        tableExamplePanel);
//    tableRowDragController.addDragHandler(demoDragHandler);

    // instantiate two flex tables
    DemoFlexTable table1 = new DemoFlexTable(5, 3, tableRowDragController);
    DemoFlexTable table2 = new DemoFlexTable(5, 4, tableRowDragController);
    tableExamplePanel.add(table1, 10, 20);
    tableExamplePanel.add(table2, 230, 40);

    // instantiate a drop controller for each table
    FlexTableRowDropController flexTableRowDropController1 = new FlexTableRowDropController(table1);
    FlexTableRowDropController flexTableRowDropController2 = new FlexTableRowDropController(table2);
    tableRowDragController.registerDropController(flexTableRowDropController1);
    tableRowDragController.registerDropController(flexTableRowDropController2);
	}
}
