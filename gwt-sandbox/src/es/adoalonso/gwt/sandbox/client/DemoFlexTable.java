package es.adoalonso.gwt.sandbox.client;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;

final class DemoFlexTable extends FlexTable {

  /**
   * Creates a FlexTable with the desired number of rows and columns, making each row draggable via
   * the provided drag controller.
   * 
   * @param rows desired number of table rows
   * @param cols desired number of table columns
   * @param tableRowDragController the drag controller to enable dragging of table rows
   */
  public DemoFlexTable(int rows, int cols, FlexTableRowDragController tableRowDragController) {
    addStyleName("demo-flextable");
    for (int row = 0; row < rows; row++) {
      HTML handle = new HTML("[drag-here]");
      handle.addStyleName("demo-drag-handle");
      setWidget(row, 0, handle);
      tableRowDragController.makeDraggable(handle);
      for (int col = 1; col < cols; col++) {
        setHTML(row, col, "[" + row + ", " + col + "]");
      }
    }
  }
}