package es.adoalonso.gwt.sandbox.client;

import com.allen_sauer.gwt.dnd.client.DragContext;
import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.allen_sauer.gwt.dnd.client.drop.BoundaryDropController;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

public final class FlexTableRowDragController extends PickupDragController {

  private static final String CSS_DEMO_FLEX_TABLE_ROW_EXAMPLE_TABLE_PROXY = "demo-FlexTableRowExample-table-proxy";

  private FlexTable draggableTable;

  private int dragRow;

  public FlexTableRowDragController(AbsolutePanel boundaryPanel) {
    super(boundaryPanel, false);
    setBehaviorDragProxy(true);
    setBehaviorMultipleSelection(false);
  }

  @Override
  public void dragEnd() {
    super.dragEnd();

    // cleanup
    draggableTable = null;
  }

  @Override
  public void setBehaviorDragProxy(boolean dragProxyEnabled) {
    if (!dragProxyEnabled) {
      // TODO implement drag proxy behavior
      throw new IllegalArgumentException();
    }
    super.setBehaviorDragProxy(dragProxyEnabled);
  }

  @Override
  protected BoundaryDropController newBoundaryDropController(AbsolutePanel boundaryPanel,
      boolean allowDroppingOnBoundaryPanel) {
    if (allowDroppingOnBoundaryPanel) {
      throw new IllegalArgumentException();
    }
    return super.newBoundaryDropController(boundaryPanel, allowDroppingOnBoundaryPanel);
  }

  @Override
  protected Widget newDragProxy(DragContext context) {
    FlexTable proxy;
    proxy = new FlexTable();
    proxy.addStyleName(CSS_DEMO_FLEX_TABLE_ROW_EXAMPLE_TABLE_PROXY);
    draggableTable = (FlexTable) context.draggable.getParent();
    dragRow = getWidgetRow(context.draggable, draggableTable);
    FlexTableUtil.copyRow(draggableTable, proxy, dragRow, 0);
    return proxy;
  }

  FlexTable getDraggableTable() {
    return draggableTable;
  }

  int getDragRow() {
    return dragRow;
  }

  private int getWidgetRow(Widget widget, FlexTable table) {
    for (int row = 0; row < table.getRowCount(); row++) {
      for (int col = 0; col < table.getCellCount(row); col++) {
        Widget w = table.getWidget(row, col);
        if (w == widget) {
          return row;
        }
      }
    }
    throw new RuntimeException("Unable to determine widget row");
  }
  
  /**
   * Utility class to manipulate {@link FlexTable FlexTables}.
   */
  public static class FlexTableUtil {

    /**
     * Copy an entire FlexTable from one FlexTable to another. Each element is copied by creating a
     * new {@link HTML} widget by calling {@link FlexTable#getHTML(int, int)} on the source table.
     * 
     * @param sourceTable the FlexTable to copy a row from
     * @param targetTable the FlexTable to copy a row to
     * @param sourceRow the index of the source row
     * @param targetRow the index before which to insert the copied row
     */
    public static void copyRow(FlexTable sourceTable, FlexTable targetTable, int sourceRow,
        int targetRow) {
      targetTable.insertRow(targetRow);
      for (int col = 0; col < sourceTable.getCellCount(sourceRow); col++) {
        HTML html = new HTML(sourceTable.getHTML(sourceRow, col));
        targetTable.setWidget(targetRow, col, html);
      }
      copyRowStyle(sourceTable, targetTable, sourceRow, targetRow);
    }

    /**
     * Move an entire FlexTable from one FlexTable to another. Elements are moved by attempting to
     * call {@link FlexTable#getWidget(int, int)} on the source table. If no widget is found (because
     * <code>null</code> is returned), a new {@link HTML} is created instead by calling
     * {@link FlexTable#getHTML(int, int)} on the source table.
     * 
     * @param sourceTable the FlexTable to move a row from
     * @param targetTable the FlexTable to move a row to
     * @param sourceRow the index of the source row
     * @param targetRow the index before which to insert the moved row
     */
    public static void moveRow(FlexTable sourceTable, FlexTable targetTable, int sourceRow,
        int targetRow) {
      if (sourceTable == targetTable && sourceRow >= targetRow) {
        sourceRow++;
      }
      targetTable.insertRow(targetRow);
      for (int col = 0; col < sourceTable.getCellCount(sourceRow); col++) {
        Widget w = sourceTable.getWidget(sourceRow, col);
        if (w != null) {
          targetTable.setWidget(targetRow, col, w);
        } else {
          HTML html = new HTML(sourceTable.getHTML(sourceRow, col));
          targetTable.setWidget(targetRow, col, html);
        }
      }
      copyRowStyle(sourceTable, targetTable, sourceRow, targetRow);
      sourceTable.removeRow(sourceRow);
    }

    /**
     * Copies the CSS style of a source row to a target row.
     * 
     * @param sourceTable
     * @param targetTable
     * @param sourceRow
     * @param targetRow
     */
    private static void copyRowStyle(FlexTable sourceTable, FlexTable targetTable, int sourceRow,
        int targetRow) {
      String rowStyle = sourceTable.getRowFormatter().getStyleName(sourceRow);
      targetTable.getRowFormatter().setStyleName(targetRow, rowStyle);
    }

  }
}